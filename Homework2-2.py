print("課題4-1")
import sys
def fibo(n):
    if n == 1:
        return 1
    elif n == 2:
        return 1
    return fibo(n-1)+fibo(n-2)

print(fibo(int(sys.argv[1])))


print("課題4-2")
def fibo_2(n):
    if n == 1:
        return 1
    elif n ==2:
        return 1
    else:
        x = 1
        y = 1
        for i in range(n-2):
            z = x + y
            x = y
            y = z
        return z

print(fibo_2(int(sys.argv[1])))


print("課題4-3")
print("再帰呼び出しを用いた場合、関数を何回も呼び出すので実行速度が遅いのに対し\
、再帰呼び出しを用いない場合は関数の呼び出しが１回だけなので実行速度が速い。")



print("課題５")
def gcd( x, y ):   #再帰を用いない方法
    while y != 0:
        x, y = y, x%y
    return x

print(gcd(42,35))


def euclid(a,b): #再帰を用いようとしたがうまく動作しない
    if a == 1 or b == 1:
        print("最大公約数は1です")
        return 0
    elif a > b: #２回め以降の再帰では必ずこちらの分岐が成り立つようにしている
        if a % b == 0:
            return b
        else:
            euclid(b,a % b)
    elif b > a:   #最初の引数は2数のどちらが大きいかわからないので場合分けする
        if b % a == 0:
            return a
        else:
            euclid(a,b % a)
    else:
        return 0

print(euclid(42,35))


print("課題6")#エラストテネスの篩
import math
sosu_list = [] #sosu_listはerast関数の外に作っておく

def erast(n):
    sqrt_int = int(math.sqrt(n)) #√n以下の最大の整数をsqrt_intに代入
    if sqrt_int <= 10:
        sosu_list.append(2)
        sosu_list.append(3)
        sosu_list.append(5)
        sosu_list.append(7)
    else:
        erast(sqrt_int) #sqrt_intが10以下でない場合、erast関数を再帰的に呼び出す\\



    number_n = []   #これ以降の行は、erast関数が再帰の底に到達して初めて実行される\\

    for i in range(2,n+1): #2以上n以下の整数を全てnumber_nに入れる
        number_n.append(i)

    for i in sosu_list: #sosu_listに入っている整数（素数）の倍数をnumber_nから取り除く\\
        for j in number_n:
            if j % i == 0:
                number_n.remove(j)

    for num in number_n: #sosu_listに入っていない素数を加える
        if num not in sosu_list:
            sosu_list.append(num)

            

print("10000までの素数は以下の通りです")
for i in sosu_list:
    print(i)
count = len(sosu_list)
print("10000までの素数の個数は",count,"個です")


import time
t0 = time.clock()   # 処理前の時刻(t0)を取得
erast(10000)       # 計測したい処理
print("10000までの素数は以下の通りです")
for i in sosu_list:
    print(i)
count = len(sosu_list)
print("10000までの素数の個数は",count,"個です")
t1 = time.clock()   # 処理後の時刻(t1)を取得


def sosu():
    b=0
    for i in range(2,10001):
        a=1
        for j in range(2,i):
            if i % j != 0:
                a *= 1
            elif i % j == 0:
                a *= 0
        if a == 1:
            print(i)
            b += 1            
    print("1000までの素数は",b,"個です")
t2 = time.clock()   # 処理前の時刻(t0)を取得
sosu()       # 計測したい処理
t3 = time.clock()   # 処理後の時刻(t1)を取得
print("前回の方法dt="+str(t3-t2)+"[s]")   # 処理後の時刻(t1)-処理前の時\\刻(t0)で処理時\\間を計算
print("エラストテネスdt="+str(t1-t0)+"[s]")   # 処理後の時刻(t1)-処理前の時刻(t\0)で処理時\\間を計算
