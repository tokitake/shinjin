#coding: utf-8
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

list1 =[]
dic ={}
data = ""

for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            if mrph.genkei not in dic:
                dic[mrph.genkei]=1
            elif mrph.genkei in dic:
                dic[mrph.genkei]+=1
        data = ""       
list1 = sorted(dic.items(),key = lambda x : x[1])
for tuple1 in list1:
    print(tuple1)
