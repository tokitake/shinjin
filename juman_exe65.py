#coding: utf-8
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

count1=0
count2=0
data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            count1+=1
            if mrph.hinsi == "動詞" or mrph.hinsi =="ナ型容詞" or mrph.hinsi =="イ型容詞":
                count2 +=1
                
        data = ""

print(count2/count1)        
