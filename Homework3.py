import sys

dic1 = {}  #単語をキーとしてその登場回数をカウント
dic2 = {}  #２gramのタプルをキーとしてその登場回数をカウント
word_sum = 0　#全単語数を計算


with open(sys.argv[1],'r') as f:
    text_list = f.readlines()
    for line in text_list:
        if line.startswith("<PAGE  URL ="):　　　#<PAGE URL = を含む行を削除
            text_list.remove(line)
            continue
        elif line.startswith("<PAGE>"):
            text_list.remove(line)
            continue
        else:
            word_list = line.split()
            for word in word_list:
                if word not in dic1:
                    dic1[word] = 1
                elif word in dic1:
                    dic1[word] += 1

            bigram_list = zip(word_list,word_list[1:])
            for words_tuple in bigram_list:
                if words_tuple not in dic2:
                    dic2[words_tuple] = 1
                elif words_tuple in dic2:
                    dic2[words_tuple] += 1

                    
def estimate():
    prob_list = []  #最初の単語の登場確率と2gramの登場確率のリスト
    p = 1           #リストの確率を掛け合わせるための初期値
    s = 0           #2gramの登場回数を数え合わせるための変数
    number_sum = 0  #全単語の登場回数を足し合わせる
    for num in dic1.values():
        number_sum += num
        
    str = "The man is in the house."
    str = str.split()
    tuple_list = zip(str,str[1:])

    for tuple in tuple_list:
        for tuple2 in dic2:
            if tuple2[0] == tuple[0]:  #タプルの最初の要素が同じものの登場回数を数える
                s += dic2[tuple2]
        if s != 0:        
            prob_list.append(dic2[tuple]/s)

    prob_list.insert(0,dic1[str[0]]/number_sum) #例文の最初の単語の登場確率をprob_listxの要素に加える

    for num in prob_list:
        p *= num

    return p


print(estimate())
