import sys
from pyknp import Jumanpp
juman = Jumanpp()

input_sentence = sys.stdin.readline()

result = juman.analysis(input_sentence)

print(" ".join(mrph.midasi for mrph in result.mrph_list()))
