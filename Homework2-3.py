print("課題7 バブルソート")
def bable(list):
    for i in range(len(list)-1,0,-1):
        for j in range(0,i-1):
            if list[j] > list[j+1]:
                t = list[j]
                list[j] = list[j+1]
                list[j+1] = t
    return list            

number_list = [5,8,2,9,11,32]

print(bable(number_list))


print("課題7 クイックソート")
def quick(list):
    r_list = []
    l_list = []
    p_list=[]
    
    if len(list) < 1:
        return list
    
    p = list[0]
    p_list.append(p)
    list.remove(p)
    
    for number in list:
        if number >= p:
            r_list.append(number)
        else: l_list.append(number)    
    r_list = quick(r_list)
    l_list = quick(l_list)

    return l_list + p_list  + r_list

print(quick(number_list))



print("課題7 マージソート")
def merge(list):
    list1 = []
    list2 = []
    new_list = []
    if len(list) <= 1:
        return list
        
    m = len(list) // 2
    list1 = list[0:m]
    list2 = list[m:]

    list1 = merge(list1)
    list2 = merge(list2)

    r_ind = 0
    l_ind = 0

    while l_ind < len(list1) and r_ind < len(list2):
        if list1[l_ind] < list2[r_ind]:
            new_list.append(list1[l_ind])
            l_ind += 1
        else :
            new_list.append(list2[r_ind])
            r_ind += 1

    if list1:
        new_list.extend(list1[l_ind:])
    if list2:
        new_list.extend(list2[r_ind:])
        
    return new_list

print(merge(number_list))
