#coding: utf-8
from pyknp import Jumanpp
import sys
import re

jumanpp = Jumanpp()

data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        mrphlist = result.mrph_list()[:]
        count = 0
        for mrph in mrphlist:
            count += 1
            if mrph.midasi == "の":
                index_no = count - 1
                if result.mrph_list()[index_no-1].hinsi == "名詞" and result.mrph_list()[index_no+1].hinsi == "名詞":
                    print(mrphlist[index_no-1].midasi + mrphlist[index_no].midasi+mrphlist[index_no+1].midasi)
        data = ""

        
