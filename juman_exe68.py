#coding: utf-8
from pyknp import KNP
import sys

knp = KNP(jumanpp=True)

data = sys.stdin.readline()
result = knp.parse(data)
bnstlist = []
for bnst in result.bnst_list():  #文節の個数だけ繰り返す
       bnstlist.append( "".join(mrph.midasi for mrph in bnst.mrph_list()))

print(" ".join(bnstlist))
