print("課題１")
def find_max(a,b,c):
    max = 1
    list = [a,b,c]
    for i in list:
         if i >= max:
             max = i

    print(max)

find_max(1,4,5)


print("課題2")
def find_max_index(list):
    max = 1  #最大値を更新するための変数
    count = 0 #インデックスを知るための変数
    for num in list:
        if max <= num:
           max = num
           iden = count  #idenは最大値のインデックスを保存しておくための変数
        count += 1
    print("Listの最大値は",max,"で、そのインデックスは",iden,"です。")


number = [1,5,4,10,7,9,8]
find_max_index(number)


print("課題3")
def replace(list):
    count=0
    for i in list:
        if count >= 1:    #listのインデックスが１以上のときに０に置換する
            list[count]=0
        count += 1
    print(list)

number = [5,6,80,2,4]
replace(number)
