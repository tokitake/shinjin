#coding: utf-8
from pyknp import KNP
import sys

knp = KNP(jumanpp=True)
data = ""
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        mrphlist = []
        print(data)
        for bnst in result.bnst_list():
            for mrph in bnst.mrph_list():
                if "<自立>" in mrph.fstring:
                    mrphlist.append(mrph)
                elif "<自立>" not in mrph.fstring:
                    if len(mrphlist) >= 2:
                        print("".join(mrph.midasi for mrph in mrphlist))
                    mrphlist =[]
        data = ""
