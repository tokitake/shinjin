#coding: utf-8
from pyknp import KNP
import sys

knp = KNP(jumanpp=True)
data = ""
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            list1 = bnst.mrph_list()
            if list1[-1].midasi == "の" and list1[-2].hinsi == "名詞":
                if bnst.bnst_id + 1 != bnst.parent.bnst_id:
                    child_rep = " ".join(mrph.repname for mrph in bnst.mrph_list())
                    parent_rep = " ".join(mrph.repname for mrph in bnst.parent.mrph_list())
                    c = result.bnst_list().index(bnst)
                    print("".join(mrph.midasi for mrph in bnst.mrph_list()))
                    print("".join(mrph.midasi for mrph in result.bnst_list()[c+1].mrph_list()))      
                    print(child_rep, "->", parent_rep)
        
        data = ""
