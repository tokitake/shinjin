#coding: utf-8
from pyknp import KNP
import sys

knp = KNP(jumanpp=True)
data = ""
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            x = 0
            y = 0
            for mrph in bnst.mrph_list():
                if mrph.hinsi == "名詞":
                    x = 1
                elif mrph.hinsi == "接尾辞":
                    y = 1
            if x == 1 and y == 1:
                print("".join(mrph.midasi for mrph in bnst.mrph_list()))
        data = ""
